import React from 'react';
//components
import ShoppingCart from './components/ShoppingCart';
import ProductsList from './components/ProductsList';
import ExersizeDescription from './components/ExersizeDescription';

class App extends React.Component {
  render() {
    return (
      <div className="Main">
        <div className="container">
          <div className="row">
            <ProductsList />
            <ShoppingCart />
            <ExersizeDescription />
          </div>
        </div>
      </div>
    );
  }
}

export default App;
