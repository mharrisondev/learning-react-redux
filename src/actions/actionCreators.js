// add product to shopping cart
export function addToCart(name, price, quantity, lineTotal) {
  return {
    type: 'ADD_TO_CART',
    name,
    price,
    quantity,
    lineTotal
  }
}

// remove one quantity from shopping cart
export function decrementQuantity(name, quantity) {
  return {
    type: 'DECREMENT_CART_ITEM',
    name,
    quantity
  }
}
