import React from 'react';

class ExersizeDescription extends React.Component {
  render () {
    return (
      <div className="col-12">
        <div className="p-3">
          <h1>Exersize Instructions</h1>
          <ul>
            <li>Write a basic Shopping Cart in React & Redux</li>
            <li>Use the given array of Products, do not modify syntax</li>
            <li>The cart will need to keep its state during page loads/refreshes</li>
            <li>Products should be listed at all times and allow adding of products</li>
            <li>Products should be listed in this format: Product Name, Price, Link to add product</li>
            <li>Cart items should be listed in this format: Product Name, Price, Quantity, Total, Link to remove</li>
            <li>Must be able to add product to the cart</li>
            <li>Must be able to view products current in the cart</li>
            <li>Product totals should be tallied to give overall total</li>
            <li>Must be able to remove a product from the cart</li>
            <li>Adding an existing product will only update the existing product quantity (Not create two cart items of same product)</li>
            <li>All prices displayed with 2 decimal places</li>
          </ul>
        </div>
      </div>
    );
  }
}
export default ExersizeDescription;
