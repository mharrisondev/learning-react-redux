import React from 'react';
import { PropTypes } from 'prop-types';

class ProductListItem extends React.Component {
  handleAddToCart(e) {
    // check for data
    if (this.props.name && this.props.formattedPrice) {
      // dispatch redux action
      return this.props.addToCart(
        this.props.name,
        this.props.formattedPrice,
      );
    }
  }

  render () {
    return(
      <tr>
        <td>{this.props.name}</td>
        <td>${this.props.formattedPrice.toFixed(2)}</td>
        <td>
          <button className="btn btn-sm btn-primary" onClick={(e) => this.handleAddToCart(e)}>Add</button>
        </td>
      </tr>
    )
  }
}

ProductListItem.propTypes = {
  name: PropTypes.string.isRequired,
  formattedPrice: PropTypes.number.isRequired,
  addToCart: PropTypes.func.isRequired
};

export default ProductListItem;
