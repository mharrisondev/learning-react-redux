import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actionCreators from '../actions/actionCreators';
import ProductListItem from './ProductListItem';

function mapStateToProps(state) {
  return {
    products: state.products,
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(actionCreators, dispatch);
}

class ProductsList extends React.Component {
  render() {
    var products = this.props.products;
    var productRows = [];

    for (var key in products) {
      if (products.hasOwnProperty(key)) {

        productRows.push(
          <ProductListItem
            key={products[key].name}
            name={products[key].name}
            formattedPrice={Number.parseFloat(products[key].price)}
            addToCart={this.props.addToCart} />
        )
      }
    }

    return (
      <div className="col-12 col-lg-6">
        <div className="products-list p-3">
          <h5>Products List</h5>
          <table className="table">
            <thead>
              <tr>
                <th scope="col">Product</th>
                <th scope="col">Price</th>
                <th scope="col"></th>
              </tr>
            </thead>
            <tbody>
              {productRows}
            </tbody>
          </table>
        </div>
      </div>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductsList);
