import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actionCreators from '../actions/actionCreators';
import ShoppingCartItem from './ShoppingCartItem';

function mapStateToProps(state) {
  return {
    shoppingCart: state.shoppingCart,
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(actionCreators, dispatch);
}

class ShoppingCart extends React.Component {
  render() {
    var shoppingCartItems = this.props.shoppingCart,
        shoppingCartRows = [],
        combinedTotal = 0;

    for (var key in shoppingCartItems) {
      if (shoppingCartItems.hasOwnProperty(key)) {

        shoppingCartRows.push(
          <ShoppingCartItem
            key={shoppingCartItems[key].name}
            name={shoppingCartItems[key].name}
            price={shoppingCartItems[key].price}
            quantity={shoppingCartItems[key].quantity}
            lineTotal={shoppingCartItems[key].lineTotal}
            decrementQuantity={this.props.decrementQuantity} />
        )
        combinedTotal = combinedTotal + Number.parseFloat(shoppingCartItems[key].lineTotal);
      }
    }

    combinedTotal = Number.parseFloat(combinedTotal).toFixed(2);

    return(
      <div className="col-12 col-lg-6">
        <div className="shopping-cart p-3">
          <h5>Shopping Cart</h5>
          <table className="table">
            <thead>
              <tr>
                <th scope="col">Product</th>
                <th scope="col">Price</th>
                <th scope="col">Quantity</th>
                <th scope="col">Total</th>
                <th scope="col"></th>
              </tr>
            </thead>
            <tbody>
              {shoppingCartRows}
            </tbody>
          </table>
          <div className="d-flex justify-content-end">Combined Total: ${combinedTotal}</div>
        </div>
      </div>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ShoppingCart);
