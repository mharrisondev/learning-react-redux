import React from 'react';
import { PropTypes } from 'prop-types';

class ShoppingCartItem extends React.Component {

  handleRemoveFromCart(e) {
      return this.props.decrementQuantity(
        this.props.name,
        this.props.quantity,
      );
  }

  render() {
    return(
      <tr>
        <td>{this.props.name}</td>
        <td>${Number.parseFloat(this.props.price).toFixed(2)}</td>
        <td>x{this.props.quantity}</td>
        <td>${Number.parseFloat(this.props.lineTotal).toFixed(2)}</td>
        <td>
          <button className="btn btn-sm btn-danger" onClick={(e) => this.handleRemoveFromCart(e)}>Remove x1</button>
        </td>
      </tr>
    )
  }
}

ShoppingCartItem.propTypes = {
  name: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  quantity: PropTypes.number.isRequired,
  lineTotal: PropTypes.number.isRequired,
  decrementQuantity: PropTypes.func.isRequired
};

export default ShoppingCartItem;
