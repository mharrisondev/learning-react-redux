import { combineReducers } from "redux";
import products from "./products";
import shoppingCart from "./shoppingCart";

//Note: every time you dispatch an action, every reducer will run
const rootReducer = combineReducers({
  products,
  shoppingCart
});

export default rootReducer;
