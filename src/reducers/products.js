/*
While this reducer has no functionality, it is required to allow the static product import to function.
It is used to mapStateToProps() on the product list
*/
function products(state = [], action) {
  return state;
}

export default products;
