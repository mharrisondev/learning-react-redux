// state = []; becuase on first run it requires a value
function shoppingCart(state = [], action) {

  let newState = [...state],
      requestedItemExists = false;

  /* Check current shopping cart state for product of the same name */
  let checkForExisting = (state) => {
    state.forEach(function(item, index) {
      for (var key in item) {
        if (item.hasOwnProperty(key)) {
          if (item[key] === action.name) {
            requestedItemExists = true;
          }
        }
      }
    });
  }

  switch (action.type) {
    case 'ADD_TO_CART':
      // check if requested item exists in cart
      if (state) {
        checkForExisting(state);
      } else {
        return state;
      }

      // update existing cart item, or add new cart item
      if (requestedItemExists) {
        newState.forEach(function(ele, index) {
          if (ele.name === action.name) {
            ele.quantity++;
            ele.lineTotal = Number.parseFloat(ele.quantity * ele.price)
          }
          return newState;
        });
      } else {
        return state.concat([{
          'name': action.name,
          'price': action.price,
          'quantity': 1,
          'lineTotal': action.price
        }]);
      }
      return newState ? newState : state;

    case 'DECREMENT_CART_ITEM':
      // check if requested item exists in cart
      if (state) {
        checkForExisting(state);
      } else {
        return state;
      }
      // update existing cart quantity, or remove if quantity is zero
      if (requestedItemExists && action.quantity) {
        if (action.quantity === 1) {
          // remove from cart
          newState.forEach(function(ele, index) {
            if (ele.name === action.name) {
              return newState.splice(index, 1);
            }
          });
        } else {
          // remove one from quantity
          newState.forEach(function(ele, index) {
            if (ele.name === action.name) {
              ele.quantity--;
              ele.lineTotal = Number.parseFloat(ele.quantity * ele.price).toFixed(2)
            }
            return newState;
          })
        }
      }
      return newState;
    default:
      return state;
  }
}

export default shoppingCart;
