import { createStore } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage'
import autoMergeLevel1 from 'redux-persist/lib/stateReconciler/autoMergeLevel2'
import rootReducer from './reducers/index';
import products from './data/products';

// create object for default data
const defaultState = {
  products: products,
  shoppingCart: []
}

// redux-persist config
const persistConfig = {
  key: 'root',
  storage,
  stateReconciler: autoMergeLevel1,
}

const persistedReducer = persistReducer(persistConfig, rootReducer)

/* store before introducing redux-persist */
// const store = createStore(
//   rootReducer,
//   defaultState,
//   window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
// );

let store = createStore(persistedReducer, defaultState, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());
let persistor = persistStore(store);

export {store, persistor};
